import argparse
import time
import cv2
import numpy as np
import json
import copy
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

from Ui_MainWindow import Ui_MainWindow
import threading
from sellerEstimator import sellerEstimator
from settingsWorker import settingsWorker

class Worker(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

    '''
    Start of work
    '''
    def doEstimate(self):
        self.event = threading.Event()
        self.event.set()
        self.doPreloader()
        self.btn_work.hide()
        self.btn_stopwork.show()
        self.showHideAllObject(False)
        self.btn_stopwork.setEnabled(True)
        self.thread_estimate = threading.Thread(target=self.threadEstimate, daemon=True, args=(self.event,self.event_preloader, False))
        self.thread_estimate.start()

    '''
    Set default settings
    '''
    def setSettings(self):
        color_range = settingsWorker().getSettings()["color_range"]
        self.choose_color_range.setValue(color_range)
        self.color_range.setText(str(color_range))
        self.relation.setCurrentIndex(settingsWorker().getSettings()["relation"])

    '''
    Changing the maximum number of customers
    '''
    def relationChange(self, value):
        settingsWorker().setRelation(value)

    '''
    Changing the camera
    '''
    def cameraChange(self, value):
        settingsWorker().setCamera(value)

    '''
    Setting the error
    '''
    def sliderChange(self):
        value = self.choose_color_range.value()
        self.color_range.setText(str(value))
        settingsWorker().setColorRange(value)

    '''
    Getting the list of active cameras
    '''
    def getCameras(self):
        number = 0
        cap = cv2.VideoCapture()
        while True:
            cap.open(number)
            if not cap.isOpened():
                break
            else:
                number += 1
                self.chooseCamera.addItem("Камера "+str(number))
                cap.release()

    '''
    Image output
    '''
    def setImage(self, image, is_defaut = False):
        height, width = image.shape[:2]
        if not is_defaut:
            new_width = 800
            new_height = int(new_width*height/width)
            image = cv2.resize(image, (new_width, new_height))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = QtGui.QImage(image, image.shape[1], image.shape[0], image.shape[1] * 3, QtGui.QImage.Format_RGB888)
        pix = QtGui.QPixmap(image)
        self.CameraImage.setPixmap(pix)

    '''
    Demonstration of the number of employees / customers
    '''
    def setClientSellerCount(self, clientCount, sellerCount):
        self.client_count.setText(str(clientCount))
        self.seller_count.setText(str(sellerCount))

    '''
    Starting the preloader
    '''
    def doPreloader(self):
        self.event_preloader = threading.Event()
        self.event_preloader.set()
        self.thread_preloader = threading.Thread(target=self.startPreloader, daemon=True, args=(self.event_preloader,))
        self.thread_preloader.start()

    '''
    Start training mode
    '''
    def learnEstimate(self):
        self.event = threading.Event()
        self.event.set()
        self.showHideAllObject(False)
        self.doPreloader()
        self.btn_stoptraining.setEnabled(True)
        self.btn_training.hide()
        self.btn_stoptraining.show()
        self.thread_estimate = threading.Thread(target=self.threadEstimate, daemon=True,args=(self.event, self.event_preloader, True))
        self.thread_estimate.start()

    '''
    Threading for recognition
    '''
    def threadEstimate(self, event, event_preloader, training):
        cap = cv2.VideoCapture("../examples/1.mp4")  # "3.mp4" #settingsWorker().getSettings()["camera"]
        seller = sellerEstimator()
        seller.workThread(cap, self, event, event_preloader, training)
        cap.release()

    '''
    Set the default image
    '''
    def setDeafautImage(self):
        image = cv2.imread("../img/screen.jpg")
        self.setImage(image, True)

    '''
    Stop recognition mode
    '''
    def stopEstimate(self):
        self.event.clear()
        if self.thread_estimate.isAlive():
            self.thread_estimate.join()
        self.event_preloader.clear()
        self.showHideAllObject(True)
        self.btn_work.show()
        self.btn_training.show()
        self.btn_stoptraining.hide()
        self.btn_stopwork.hide()
        if self.thread_preloader.isAlive():
            self.thread_preloader.join()
        self.setDeafautImage()

    '''
    preloader animation
    '''
    def startPreloader(self, event_preloader):
        event_preloader.wait()
        counter = 0
        while (event_preloader.is_set()):
            image=cv2.imread("../img/preloader2/"+str(counter)+".jpg")
            self.setImage(image, True)
            counter += 1
            if counter > 103:
                counter = 0
            time.sleep(0.03)

if __name__ == '__main__':
    app = QtWidgets.QApplication(['output2.py'])
    MainWindow = QtWidgets.QMainWindow()

    form = Worker()
    form.show()

    app.exec_()