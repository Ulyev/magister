from pose_estimator.estimator import TfPoseEstimator
from pose_estimator.networks import get_graph_path, model_wh
import cv2
import numpy as np
import copy
import threading
from settingsWorker import settingsWorker
import time
import winsound

class sellerEstimator(object):
    def workThread(self, cap, ui, event, event_preloader, training):
        event.wait()
        e = self.setEstimator()
        if (cap.isOpened() == False):
            print("Error opening video stream or file")

        event_preloader.clear()

        colors = []
        if not training:
            color_area  = self.getColorArea(settingsWorker().getSettings()["color_interval"])

        clientList = []
        sellerList = []
        time_b = time.time()

        iter = 0


        while (cap.isOpened() and event.is_set()):
            ret_val, image = cap.read()
            try:
                if training:
                    colors.extend(self.startEstimator(image, e, ui, True, False))
                else:
                    peoples = self.startEstimator(image, e, ui, False, color_area)
                    iter += 1
                    clientList.append(peoples[0])
                    sellerList.append(peoples[1])
                    if time.time() - time_b > 30:
                        threading.Thread(target=self.voiceNotification, daemon=True, args=(clientList, sellerList)).start()
                        time_b = time.time()
                        clientList.clear()
                        sellerList.clear()
                    ui.setClientSellerCount(peoples[0], peoples[1])
            except BaseException as error:
                if ("The image is not valid" in str(error)):
                    print("ERROR " + str(error))
                    break
        if training:
            settingsWorker().setColorInterval(self.getAveargeColor(colors))

    '''
    Playing the voice command
    '''
    def voiceNotification(self, clientList, sellerList):
        meanSeller = np.mean(sellerList)
        meanClient = np.mean(clientList)

        if meanSeller==0 and meanClient!=0:
            winsound.PlaySound('../audio/call_seller.wav', winsound.SND_FILENAME)
        elif meanSeller!=0 and meanClient!=0:
            relation = settingsWorker().getSettings()["relation"]
            if (round(meanClient/meanSeller)<(relation+1)):
                winsound.PlaySound('../audio/call_seller.wav', winsound.SND_FILENAME)

    def startEstimator(self, image, e, ui, training, color_area):
        # image2 = copy.copy(image)
        colors = []
        humans = e.inference(image)
        image = TfPoseEstimator.draw_humans(image, humans, imgcopy=False)
        ui.setImage(image)
        bodies = self.getBodies(humans, image)
        client = 0
        seller = 0

        for body in bodies:
            rectBody = self.getRectbody(body, image)
            if not rectBody:
                continue
            cutImg = self.cutImg(image, rectBody)
            color = self.dominantImgColor(cutImg)
            if training:
                colors.append(color)
            else:
                if self.areaHaveColor(color, color_area):
                    seller += 1
                else:
                    client += 1
        if training:
            return colors
        else:
            return [client, seller]

    '''
    Set the pose-estimator
    '''
    def setEstimator(self):
        model = "mobilenet_thin"  # mobilenet_thin # cmu
        resolution = "640x480" #640x480
        w, h = model_wh(resolution)
        e = TfPoseEstimator(get_graph_path(model), target_size=(w, h))
        return e

    '''
    Searching for bodies
    '''
    def getBodies(self, humans, npimg):
        image_h, image_w = npimg.shape[:2]
        bodies = []

        elements = [1, 2, 5, 8, 11]
        for human in humans:
            body = {}
            for i in elements:
                if i not in human.body_parts.keys():
                    body[i] = False
                    continue

                body_part = human.body_parts[i]
                body[str(i)] = [body_part.x*image_w, body_part.y*image_h]
            bodies.append(body)
        return bodies

    '''
    Getting the trunk coordinates
    '''
    def getRectbody(self, body, image):
        height, width = image.shape[:2]
        if all(k in body.keys() for k in ('1', '8', '11')) and not self.zero_area([min(body['11'][0], body['8'][0]), body['1'][1]],[max(body['8'][0], body['11'][0]), body['8'][1]]):
            left_top_x = min(body['11'][0], body['8'][0])
            left_top_y =  body['1'][1]
            right_bottom_x = max(body['8'][0], body['11'][0])
            right_bottom_y = body['8'][1]
        else:
            if all(k in body.keys() for k in ('1', '11')) and not self.zero_area(body['1'], body['11']):
                point1 = body['1']
                point2 = body['11']
            elif all(k in body.keys() for k in ('1', '8')) and not self.zero_area(body['1'], body['8']):
                point1 = body['1']
                point2 = body['8']
            elif all(k in body.keys() for k in ('5', '1')) and not self.zero_area(body['1'], body['5']):
                point1 = body['1']
                point2_y = body['1'][1] + (max(body['1'][0], body['5'][0]) - min(body['1'][0], body['5'][0]))*2
                if point2_y > height:
                    point2_y = height
                point2 = [body['5'][0], point2_y]
            elif all(k in body.keys() for k in ('1')):
                left_top_x = body['1'][0]-25
                left_top_y = body['1'][1]
                right_bottom_x = body['1'][0]+25
                right_bottom_y = body['1'][1]+50

                if left_top_x < 0 or right_bottom_x > width or right_bottom_y > height:
                    return False
                left_top_p = [left_top_x, left_top_y]  # верхняя левая точка
                right_bottom_p = [right_bottom_x, right_bottom_y]  # правая нижняя точка
                return [left_top_p, right_bottom_p]
            else:
                return False

            if point2[0] > point1[0]:  # если точка 11 правее точки 1
                if point1[0] - (point2[0] - point1[0]) >= 0:  # если может отложить равное плечо влево
                    left_top_x = point1[0] - (point2[0] - point1[0])
                else:
                    left_top_x = 0
                left_top_y = point1[1]
                right_bottom_x = point2[0]
            else:
                left_top_x = point2[0]
                left_top_y = point1[1]
                if point1[0] + (point1[0] - point2[0]) < width:  # если можем отложить равное плечо вправо
                    right_bottom_x = point1[0] - (point2[0] - point1[0])
                else:
                    right_bottom_x = width
            right_bottom_y = point2[1]

        left_top_p = [left_top_x, left_top_y]  # верхняя левая точка
        right_bottom_p = [right_bottom_x, right_bottom_y]  # правая нижняя точка
        return [left_top_p, right_bottom_p]

    '''
    Check for emptiness
    '''
    def zero_area(self, point1, point2):
        if abs(point1[0] - point2[0]) < 10 or abs(point1[1] - point2[1]) < 10:
            return True
        else:
            return False

    '''
    Image Cropping
    '''
    def cutImg(self, img, points):
        cut_img_r = img[int(points[0][1]):int(points[1][1]), int(points[0][0]):int(points[1][0])]
        return cut_img_r

    '''
    Search for the dominant color
    '''
    def dominantImgColor(self, img):
        width, height, channels = img.shape
        r_total = 0
        g_total = 0
        b_total = 0
        count = 0
        for x in range(0, width):
            for y in range(0, height):
                r, g, b = img[x, y]
                r_total += r
                g_total += g
                b_total += b
                count += 1
        return (int(r_total / count), int(g_total / count), int(b_total / count))

    def getAveargeColor(self, colors):
        aveargeH = 0
        aveargeS = 0
        aveargeV = 0
        count = 0
        for bgr in colors:
            hsv = self.rgb2hsv(bgr)  # convert hsv to rgb
            aveargeH += hsv[0]
            aveargeS += hsv[1]
            aveargeV += hsv[2]
            count += 1

        aveargeH = round(aveargeH / count)
        aveargeS = round(aveargeS / count, 2)
        aveargeV = round(aveargeV / count, 2)
        return [aveargeH, aveargeS, aveargeV]

    def rgb2hsv(self, bgr):
        b = bgr[0]
        g = bgr[1]
        r = bgr[2]
        r, g, b = r / 255.0, g / 255.0, b / 255.0
        mx = max(r, g, b)
        mn = min(r, g, b)
        df = mx - mn
        if mx == mn:
            h = 0
        elif mx == r:
            h = (60 * ((g - b) / df) + 360) % 360
        elif mx == g:
            h = (60 * ((b - r) / df) + 120) % 360
        elif mx == b:
            h = (60 * ((r - g) / df) + 240) % 360
        if mx == 0:
            s = 0
        else:
            s = df / mx
        v = mx
        return h, s, v

    '''
    Formation of color range
    '''
    def getColorArea(self, hsv):
        h1 = hsv[0] + settingsWorker().getSettings()["color_range"]
        h2 = hsv[0] - settingsWorker().getSettings()["color_range"]
        if h1 > 359:
            h1 = 0

        if h2 < 0:
            h2 = 0

        s1 = hsv[1] + 0.4
        s2 = hsv[1] - 0.4
        if s1 > 1:
            s1 = 1
        if s2 < 0:
            s2 = 0

        v1 = hsv[2] + 0.2
        v2 = hsv[2] - 0.8
        if v1 > 1:
            v1 = 1
        if v2 < 0:
            v2 = 0

        return [
            [round(h1), round(s1, 2), round(v1, 2)],
            [round(h2), round(s2, 2), round(v2, 2)]
        ]

    '''
    Checking the color in the range
    '''
    def areaHaveColor(self, bgr, color_area):
        hsv = self.rgb2hsv(bgr)
        if hsv[0] > color_area[0][0] or hsv[0] < color_area[1][0]:
            return False

        if hsv[1] > color_area[0][1] or hsv[1] < color_area[1][1]:
            return False

        if hsv[2] > color_area[0][2] or hsv[2] < color_area[1][2]:
            return False

        return True