# coding=utf-8
# import the necessary packages
import numpy as np
import cv2
import copy
import matplotlib.pyplot as plt
import numpy

import scipy.cluster.hierarchy as hcluster
from sklearn.cluster import SpectralClustering
from sklearn import metrics


class DistanceDetector:
    def __init__(self):
        cap = cv2.VideoCapture(1)
        while (True):
            ret, frame = cap.read()
            save_frame = copy.copy(frame)
            ellipse_contours = self.findEllipse(frame)

            ellipse = self.clasterEllips(ellipse_contours, frame)

            rect = self.findRect(frame)
            if len(ellipse) and len(rect):
                markers = self.findEllipseInRect(ellipse, rect, save_frame)
                if len(markers) is 0:
                    print("Метка не найдена")
                else:
                    for marker in markers:
                        cv2.ellipse(frame, marker["ellipse"], (0, 0, 255), 2)
                        cv2.drawContours(frame, [marker["rect"]], -1, (0, 255, 0), 2)
            cv2.imshow("Output", frame)
            cv2.waitKey()

        cap.release()
        cv2.waitKey()
        cv2.destroyAllWindows()

    @staticmethod
    def clasterEllips(contours, image):
        if len(contours):
            coors = []
            for contour in contours:
                for pic in contour:
                    coors.append([pic[0][0], pic[0][1]])

            coors = np.array(coors)

            # иерархическая кластеризация
            thresh = 50
            clusters = hcluster.fclusterdata(coors, thresh, criterion="distance")

            claster_contours = []
            current_index = clusters[0]
            bufer_array = []
            for coor, clus in zip(coors, clusters):
                if current_index != clus:
                    current_index = clus
                    claster_contours.append(np.array(bufer_array))
                    bufer_array = []
                bufer_array.append([coor])
            claster_contours.append(np.array(bufer_array))

            plt.scatter(*np.transpose(coors), c=clusters)
            plt.axis("equal")
            title = "threshold: %f, number of clusters: %d" % (thresh, len(set(clusters)))
            plt.title(title)
            plt.show()

            ellipses = []
            contours = claster_contours
            for contour in contours:
                ellipse = cv2.fitEllipse(np.array(contour))
                ellipses.append(ellipse)
            return ellipses
        else:
            return []

    def findEllipse(self, image):
        """
        Поиск эллипса на изображении
        :param image: изображение
        :return ellipses: массив эллипсов
        """
        bilateral_filtered_image = cv2.bilateralFilter(image, 25, 255, 255)
        edge_detected_image = cv2.Canny(bilateral_filtered_image, 300, 300)
        _, contours, hierarchy = cv2.findContours(edge_detected_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        contours_arr = []
        for contour in contours:
            approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True), True)
            area = cv2.contourArea(contour)
            if (len(approx) > 8) & (len(approx) < 23) & (area > 30):
                contours_arr.append(contour)

        return contours_arr

    def findRect(self, image):
        """
        Поиск прямоугольной области на изображении
        :param image: изображение
        :return: координаты прямоуголной области
        """
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (5, 5), 0)
        edged = cv2.Canny(gray, 105, 250)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 9))
        closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
        (_, cnts, _) = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        rects = []
        for c in cnts:
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.02 * peri, True)
            if len(approx) == 4:
                rects.append(approx)

        return rects

    def findEllipseInRect(self, ellipses, rects, image):
        area = []
        for rect in rects:
            segments = [
                [{"x": rect[0][0][0], "y": rect[0][0][1]}, {"x": rect[1][0][0], "y": rect[1][0][1]}],
                [{"x": rect[1][0][0], "y": rect[1][0][1]}, {"x": rect[2][0][0], "y": rect[2][0][1]}],
                [{"x": rect[2][0][0], "y": rect[2][0][1]}, {"x": rect[3][0][0], "y": rect[3][0][1]}],
                [{"x": rect[3][0][0], "y": rect[3][0][1]}, {"x": rect[0][0][0], "y": rect[0][0][1]}]
            ]
            for ellipse in ellipses:
                select_ellipse = ellipse
                vector = [{"x": ellipse[0][0], "y": ellipse[0][1]}, {"x": ellipse[0][0], "y": 0}]
                counter = 0
                for segment in segments:
                    if self.isIntersect(segment[0], segment[1], vector[0], vector[1]):
                        counter += 1
                        if counter >= 2:
                            select_ellipse = False

                if select_ellipse is not False and counter is not 0:
                    area.append({"rect": rect, "ellipse": select_ellipse})

        return area

    def area(self, a, b, c):
        return (b["x"] - a["x"]) * (c["y"] - a["y"]) - (b["y"] - a["y"]) * (c["x"] - a["x"])

    def swap(self, s1, s2):
        return s2, s1

    def intersect(self, a, b, c, d):
        if a > b:
            a, b = self.swap(a, b)
        if c > d:
            c, d = self.swap(c, d)

        return max(a, c) <= min(b, d)

    def isIntersect(self, a, b, c, d):
        return self.intersect(a["x"], b["x"], c["x"], d["x"]) \
               and self.intersect(a["y"], b["y"], c["y"], d["y"]) \
               and self.area(a, b, c) * self.area(a, b, d) <= 0 \
               and self.area(c, d, a) * self.area(c, d, b) <= 0


DistanceDetector()
