import os
import json

class settingsWorker(object):
    def __init__(self):
        self.settings = {}
        self.settings["camera"] = 0
        self.settings["relation"] = 0
        self.settings["color_range"] = 35
        self.readSettings()

    def readSettings(self):
        if os.path.exists("../base/settings.json"):
            file = open('../base/settings.json', 'r')
            json_parse = json.loads(file.read())
            if not "color_interval" in json_parse:
                return False

            if "camera" in json_parse:
                self.settings["camera"] = json_parse["camera"]
            if "relation" in json_parse:
                self.settings["relation"] = json_parse["relation"]
            if "color_range" in json_parse:
                self.settings["color_range"] = json_parse["color_range"]
            self.settings["color_interval"] = json_parse["color_interval"]
            file.close()
            return True
        else:
            return False

    def getSettings(self):
        return self.settings

    def setCamera(self, camera):
        self.settings["camera"] = camera
        self.save()

    def setColorInterval(self, color_range):
        self.settings["color_interval"] = color_range
        self.save()

    def setRelation(self, relation):
        self.settings["relation"] = relation
        self.save()

    def setColorRange(self, color_range):
        self.settings["color_range"] = color_range
        self.save()

    def save(self):
        file = open('../base/settings.json', 'w')
        file.write(json.dumps(self.settings))
        file.close()