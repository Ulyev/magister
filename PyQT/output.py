# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'form2.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1038, 584)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.CameraImage = QtWidgets.QLabel(self.centralwidget)
        self.CameraImage.setMinimumSize(QtCore.QSize(700, 500))
        self.CameraImage.setObjectName("CameraImage")
        self.horizontalLayout.addWidget(self.CameraImage)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.chooseCamera = QtWidgets.QComboBox(self.centralwidget)
        self.chooseCamera.setObjectName("chooseCamera")
        self.chooseCamera.addItem("")
        self.verticalLayout.addWidget(self.chooseCamera)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.btn_training = QtWidgets.QPushButton(self.centralwidget)
        self.btn_training.setObjectName("btn_training")
        self.horizontalLayout_2.addWidget(self.btn_training)
        self.btn_stoptraining = QtWidgets.QPushButton(self.centralwidget)
        self.btn_stoptraining.setObjectName("btn_stoptraining")
        self.horizontalLayout_2.addWidget(self.btn_stoptraining)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.btn_work = QtWidgets.QPushButton(self.centralwidget)
        self.btn_work.setObjectName("btn_work")
        self.horizontalLayout_3.addWidget(self.btn_work)
        self.btn_stopwork = QtWidgets.QPushButton(self.centralwidget)
        self.btn_stopwork.setObjectName("btn_stopwork")
        self.horizontalLayout_3.addWidget(self.btn_stopwork)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.choose_color_range = QtWidgets.QSlider(self.groupBox)
        self.choose_color_range.setMinimum(20)
        self.choose_color_range.setMaximum(50)
        self.choose_color_range.setProperty("value", 35)
        self.choose_color_range.setOrientation(QtCore.Qt.Horizontal)
        self.choose_color_range.setObjectName("choose_color_range")
        self.gridLayout.addWidget(self.choose_color_range, 1, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)
        self.relation = QtWidgets.QComboBox(self.groupBox)
        self.relation.setObjectName("relation")
        self.relation.addItem("")
        self.relation.addItem("")
        self.relation.addItem("")
        self.relation.addItem("")
        self.relation.addItem("")
        self.gridLayout.addWidget(self.relation, 3, 0, 1, 1)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setObjectName("label")
        self.horizontalLayout_4.addWidget(self.label)
        self.color_range = QtWidgets.QLabel(self.groupBox)
        self.color_range.setToolTip("")
        self.color_range.setWhatsThis("")
        self.color_range.setStyleSheet("")
        self.color_range.setObjectName("color_range")
        self.horizontalLayout_4.addWidget(self.color_range, 0, QtCore.Qt.AlignRight)
        self.gridLayout.addLayout(self.horizontalLayout_4, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.groupBox)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.client_count = QtWidgets.QLabel(self.centralwidget)
        self.client_count.setObjectName("client_count")
        self.verticalLayout.addWidget(self.client_count)
        self.seller_count = QtWidgets.QLabel(self.centralwidget)
        self.seller_count.setObjectName("seller_count")
        self.verticalLayout.addWidget(self.seller_count)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.gridLayout_2.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1038, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.CameraImage.setText(_translate("MainWindow", "Загрузка..."))
        self.chooseCamera.setItemText(0, _translate("MainWindow", "Выберите камеру"))
        self.btn_training.setText(_translate("MainWindow", "Режим обучения"))
        self.btn_stoptraining.setText(_translate("MainWindow", "Остановить режим обучения"))
        self.btn_work.setText(_translate("MainWindow", "Режим распознования"))
        self.btn_stopwork.setText(_translate("MainWindow", "Остановить режим распознования"))
        self.groupBox.setTitle(_translate("MainWindow", "Общие настройки"))
        self.label_2.setText(_translate("MainWindow", "Соотношение продавец/покупатель"))
        self.relation.setItemText(0, _translate("MainWindow", "1 продавец - 1 покупатель"))
        self.relation.setItemText(1, _translate("MainWindow", "1 продавец - 2 покупателя"))
        self.relation.setItemText(2, _translate("MainWindow", "1 продавец - 3 покупателя"))
        self.relation.setItemText(3, _translate("MainWindow", "1 продавец - 4 покупателя"))
        self.relation.setItemText(4, _translate("MainWindow", "1 продавец - 5 покупателей"))
        self.label.setText(_translate("MainWindow", "Цветовой диапозон"))
        self.color_range.setText(_translate("MainWindow", "35"))
        self.client_count.setText(_translate("MainWindow", "ClientCount"))
        self.seller_count.setText(_translate("MainWindow", "SellerCount"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

